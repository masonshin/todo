const { resolve } = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const DashboardPlugin = require('webpack-dashboard/plugin')

const PATH = {
  src: resolve(__dirname, 'src'),
  dist: resolve(__dirname, 'dist')
}

module.exports = {
  entry: `${PATH.src}/index.js`,
  output: {
    path: PATH.dist,
    filename: '[name].js',
    sourceMapFilename: '[name].map',
    libraryTarget: 'umd',
    pathinfo: false
  },
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: true,
    compress: true,
    stats: 'errors-only',
    port: 4000
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    loaders: [
      {
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.ejs$/,
        loader: 'ejs-loader?variable=data'
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader?sourceMap', 'sass-loader?sourceMap']
        })
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: resolve(PATH.src, 'index.ejs')
    }),
    new ExtractTextPlugin('[name].css'),
    new DashboardPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
}
