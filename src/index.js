import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './containers/App'
import todo from './reducers'
import './app.scss'

const store = createStore(todo)
const rootElement = document.getElementById('mainContainer')

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
)
