import React, { Component, PropTypes } from 'react'
import cx from 'classnames/bind'
import styles from './todo'

const classNames = cx.bind(styles)

export default class Todo extends Component {
  render () {
    return (
      <li
        onClick={this.props.onClick}
        className={classNames('todo-item')}
        style={{
          textDecoration: this.props.completed ? 'line-through' : 'none',
          cursor: this.props.completed ? 'default' : 'pointer',
          color: this.props.completed ? 'silver' : 'inherit'
        }}>
          {this.props.text}
      </li>
    )
  }
}

Todo.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired
}
