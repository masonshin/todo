import React, { Component, PropTypes } from 'react'
import Todo from './Todo'
import cx from 'classnames/bind'
import styles from './todo.scss'

const classNames = cx.bind(styles)

export default class TodoList extends Component {
  render () {
    return (
      <ul className={classNames('todo-list')}>
        {this.props.todos.map((todo, index) =>
          <Todo {...todo}
            key={index}
            onClick={() => this.props.onTodoClick(index)} />
        )}
      </ul>
    )
  }
}

TodoList.propTypes = {
  onTodoClick: PropTypes.func.isRequired,
  todos: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired
  }).isRequired).isRequired
}
