import React, { Component, PropTypes } from 'react'
import { findDOMNode } from 'react-dom'
import cx from 'classnames/bind'
import styles from './addTodo.scss'

const classNames = cx.bind(styles)

export default class AddTodo extends Component {
  handleClick (e) {
    const node = findDOMNode(this.refs.input)
    const text = node.value.trim()
    this.props.onAddClick(text)
    node.value = ''
  }

  render () {
    return (
      <div>
        <input
          type='text'
          ref='input'
          placeholder='What you need to do?'
          className={classNames('todo-input')} />
        <button onClick={(e) => this.handleClick(e)} className={classNames('add-btn')}>Add</button>
      </div>
    )
  }
}

AddTodo.propTypes = {
  onAddClick: PropTypes.func.isRequired
}
